---
header-includes:
  - \usepackage{polyglossia}
  - \setdefaultlanguage{russian}
  - \setmainfont{XITS}
  - \newfontfamily{\cyrillicfont}{XITS}
  - \setsansfont{XITS}
  - \newfontfamily{\cyrillicfontsf}{XITS}
  - \setmonofont{JetBrains Mono}
  - \newfontfamily{\cyrillicfonttt}{XITS}
---
# PIMP
## О PIMP
### Что такое PIMP?

PIMP это препроцессор для markdown


### Для чего он нужен?

Для вычислений прямо в markdown файле

Например следующий фрагмент был сгенерирован прямо в тексте. Ориг можно посмотреть в [README.pimp](./README.pimp)

```
                  * 
                * * * 
              * * * * * 
            * * * * * * * 
          * * * * * * * * * 
        * * * * * * * * * * * 
      * * * * * * * * * * * * * 
    * * * * * * * * * * * * * * * 
  * * * * * * * * * * * * * * * * * 
* * * * * * * * * * * * * * * * * * * 

```


Также можно вызывать код прямо внутри строки



Типа так: <Hello **User**!>
