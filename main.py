import os
import io
import sys


sessionGlobals = {"built" : __builtins__}
sessionLocals = {}


def readFile():
    if len(sys.argv) >= 2:
        content = open(sys.argv[1], 'r').read()
    else:
        content = os.fdopen(0).read()
    return(content)

def getStartOfCodeBlock(content, start=0):
    return content.find('{{{', start)


def getEndOfCodeBlock(content, start=0):
    return content.find('}}}', start)


def runWithExec(codeBlocks):
    pipes = []
    for block in codeBlocks:
        oo, oi = os.pipe()
        eo, ei = os.pipe()
        pipes.append({'oo': oo, 'oi': oi, 'eo': eo, 'ei': ei})
        
    if (os.fork() == 0):
        for i in range(len(codeBlocks)):
            os.close(pipes[i]['oo'])
            os.close(pipes[i]['eo'])
            os.dup2(pipes[i]['oi'], 1)
            os.dup2(pipes[i]['ei'], 2)
            exec(codeBlocks[i], sessionGlobals, sessionLocals)
            sys.stdout.flush()
            os.close(pipes[i]['oi'])
            os.close(pipes[i]['ei'])
        sys.exit()
    else:
        for pipe in pipes:
            os.close(pipe['oi'])
            os.close(pipe['ei'])
        pid, status = os.waitpid(-1, 0)

    out = []
    err = []
    for pipe in pipes:
        out.append(os.fdopen(pipe['oo'], closefd=True).read())
        err.append(os.fdopen(pipe['eo'], closefd=True).read())
    return (out, err)

    
if __name__ == '__main__':
    s = 0
    e = 0
    c = readFile()
    text = []
    prog = []
    while (True):
        s = getStartOfCodeBlock(c, e)
        text.append(c[e:s])
        if (s < 0):
            break
        e = getEndOfCodeBlock(c, s)
        if (e < 0):
            break
        prog.append(c[s+3:e])
        e += 3
        
    out, err = runWithExec(prog)
    for i in range(len(text)):
        print(text[i], end='')
        if (i < len(out)):
            print(out[i], end='')
            if (err[i] != ''):
                print('===========', end='', file=sys.stderr)
                print(prog[i], file=sys.stderr)
                print(err[i], end='', file=sys.stderr)
                print('===========', file=sys.stderr)
    print()
