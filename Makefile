MAIN = README.pimp
PIMPS = 
MDS = $(MAIN:.pimp=.md) $(PIMPS:.pimp=.md)


all: README.pdf

README.pdf: $(MDS)
	pandoc $(MAIN:.pimp=.md) -o $@ --pdf-engine=xelatex

%.md: %.pimp
	python main.py $< > $@
